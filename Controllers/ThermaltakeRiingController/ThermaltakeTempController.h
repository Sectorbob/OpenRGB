/*-------------------------------------------------------------------*\
|  ThermaltakeRiingQuadController.h                                   |
|                                                                     |
|  Driver for Thermaltake Riing Quad Controller                       |
|                                                                     |
|  Chris M (Dr_No)          15th Feb 2021                             |
|                                                                     |
\*-------------------------------------------------------------------*/

#include "RGBController.h"
#include <chrono>
#include <vector>
#include <hidapi/hidapi.h>
#include <mutex>
#include <ThermaltakeHeartbeatController.h>

#pragma once

#define THERMALTAKE_TEMP_PACKET_SIZE        65
#define THERMALTAKE_TEMP_INTERRUPT_TIMEOUT  250
#define THERMALTAKE_TEMP_KEEPALIVE          500
#define HID_MAX_STR                        255

enum
{
    THERMALTAKE_TEMP_COMMAND_BYTE   = 1,
    THERMALTAKE_TEMP_FUNCTION_BYTE  = 2,
    THERMALTAKE_TEMP_ZONE_BYTE      = 3,
    THERMALTAKE_TEMP_MODE_BYTE      = 4,
    THERMALTAKE_TEMP_CHUNK_BYTE     = 6,
    THERMALTAKE_TEMP_DATA_BYTE      = 8,
};

enum
{
    THERMALTAKE_TEMP_MODE_DIRECT    = 0x18
};

#define THERMALTAKE_TEMP_NUM_CHANNELS   5

class ThermaltakeTempController : public ThermaltakeHeartbeatController
{
public:
    ThermaltakeTempController(hid_device* dev_handle, const char* path, unsigned short pid);
    ~ThermaltakeTempController();

    void Init();
    void Heartbeat();
    void SetRGB (unsigned char channel, RGBColor * colors, unsigned int num_colors);
    void SetFanSpeed(uint8_t port, uint8_t speed);
private:
    std::atomic<unsigned char> current_mode;

    std::mutex              tt_out_mutex;
    std::mutex              tt_out_mutex2;
    std::mutex              tt_in_mutex;

    uint8_t                 tt_out_buffer[THERMALTAKE_TEMP_NUM_CHANNELS][THERMALTAKE_TEMP_PACKET_SIZE];
    uint8_t                 tt_out_buffer2[THERMALTAKE_TEMP_NUM_CHANNELS][THERMALTAKE_TEMP_PACKET_SIZE];
    uint8_t                 tt_in_buffer[THERMALTAKE_TEMP_NUM_CHANNELS][THERMALTAKE_TEMP_PACKET_SIZE];

    void                    GetFirmwareVersion();
    void                    GetData(uint8_t port);
};
