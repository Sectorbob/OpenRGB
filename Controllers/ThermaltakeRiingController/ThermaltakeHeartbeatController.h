#ifndef THERMALTAKEHEARTBEATCONTROLLER_H
#define THERMALTAKEHEARTBEATCONTROLLER_H

#include <atomic>
#include <vector>
#include <string>
#include <thread>
#include <chrono>
#include <mutex>
#include <hidapi/hidapi.h>
#include "RGBController.h"


class ThermaltakeHeartbeatController
{
public:
    ThermaltakeHeartbeatController(hid_device* dev_handle, const char* path);
    ~ThermaltakeHeartbeatController();
    void Init();
    virtual void SetRGB (uint8_t channel, RGBColor * colors, unsigned int num_colors);
    void         SetMode(uint8_t mode);
    std::string  GetDeviceName();
    std::string  GetDeviceLocation();
    std::string  GetSerial();
    unsigned int NumberOfChannels();
protected:
    static const       int                             HID_MAX_STR = 255;
    static const       int                             DEFAULT_NUM_ZONES = 5;
    static constexpr   std::chrono::milliseconds       DEFAULT_HEARTBEAT_INTERVAL = std::chrono::milliseconds(1000);
    std::string                                        device_name;
    std::mutex                                         update_mutex;
    std::chrono::milliseconds                          heartbeat_interval;
    hid_device*                                        dev;
    virtual void Heartbeat();
private:
    std::string                                        serial;
    std::string                                        location;
    std::thread*                                       keepalive_thread;
    std::atomic<bool>                                  keepalive_thread_run;
    std::chrono::time_point<std::chrono::steady_clock> last_commit_time;
    unsigned char                                      current_mode;
    unsigned char                                      current_speed;
    void StartHeartbeat();
    void RunHeartbeat();
    void StopHeartbeat();
};

#endif // THERMALTAKEHEARTBEATCONTROLLER_H
