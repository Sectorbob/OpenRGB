/*-------------------------------------------------------------------*\
|  RGBController_ThermaltakeRiingQuad.cpp                             |
|                                                                     |
|  Driver for Thermaltake Riing Quad Controller                       |
|                                                                     |
|  Chris M (Dr_No)          15th Feb 2021                             |
|                                                                     |
\*-------------------------------------------------------------------*/

#include "RGBController_Thermaltake.h"
#include "QDebug"

RGBController_Thermaltake::RGBController_Thermaltake(ThermaltakeHeartbeatController* ctrl_ptr)
{
    ctrl = ctrl_ptr;

    name        = ctrl->GetDeviceName();
    vendor      = "Thermaltake";
    type        = DEVICE_TYPE_COOLER;
    description = "Thermaltake Riing Quad Device";
    location    = ctrl->GetDeviceLocation();
    serial      = ctrl->GetSerial();

    mode Direct;
    Direct.name       = "Direct";
    Direct.value      = ThermaltakeRiingQuadController::MODE_DIRECT;
    Direct.flags      = MODE_FLAG_HAS_PER_LED_COLOR;
    Direct.speed_min  = 0;
    Direct.speed_max  = 0;
    Direct.speed      = 0;
    Direct.color_mode = MODE_COLORS_PER_LED;
    modes.push_back(Direct);
}

RGBController_Thermaltake::~RGBController_Thermaltake()
{
    delete ctrl;
}

void RGBController_Thermaltake::SetupZones()
{
    //qDebug() << "Ring Quad SetupZones Start";
    resize_mutex.lock();
    /*-------------------------------------------------*\
    | Only set LED count on the first run               |
    \*-------------------------------------------------*/
    bool first_run = false;

    if(zones.size() == 0)
    {
        first_run = true;
    }

    /*-------------------------------------------------*\
    | Clear any existing color/LED configuration        |
    \*-------------------------------------------------*/
    leds.clear();
    leds_channel.clear();
    colors.clear();
    zones.resize(ctrl->NumberOfChannels());

    /*-------------------------------------------------*\
    | Set zones and leds                                |
    \*-------------------------------------------------*/
    for (unsigned int channel_idx = 0; channel_idx < ctrl->NumberOfChannels(); channel_idx++)
    {
        char ch_idx_string[2];
        sprintf(ch_idx_string, "%d", channel_idx + 1);

        zones[channel_idx].name = ctrl->GetDeviceName().append(" Channel ").append(ch_idx_string);
        zones[channel_idx].type = ZONE_TYPE_LINEAR;

        /*-------------------------------------------------*\
        | The maximum number of colors that would fit in the|
        | Riing Quad protocol is 54                         |
        \*-------------------------------------------------*/
        zones[channel_idx].leds_min   = 0;
        zones[channel_idx].leds_max   = 54;

        if(first_run)
        {
            zones[channel_idx].leds_count = 0;
        }

        zones[channel_idx].matrix_map = NULL;
        
        for (unsigned int led_ch_idx = 0; led_ch_idx < zones[channel_idx].leds_count; led_ch_idx++)
        {
            char led_idx_string[3];
            sprintf(led_idx_string, "%d", led_ch_idx + 1);

            led new_led;
            new_led.name = ctrl->GetDeviceName().append(" Channel ");
            new_led.name.append(ch_idx_string);
            new_led.name.append(", LED ");
            new_led.name.append(led_idx_string);

            leds.push_back(new_led);
            leds_channel.push_back(channel_idx);
        }
    }

    SetupColors();
    resize_mutex.unlock();
    //qDebug() << "Ring Quad SetupZones End";
}

void RGBController_Thermaltake::ResizeZone(int zone, int new_size)
{
    resize_mutex.lock();
    if((size_t) zone >= zones.size())
    {
        // do not resize for zone index out of bounds
        resize_mutex.unlock();
        return;
    }

    if (zones[zone].leds_count == new_size)
    {
        // do not resize if zone is already correct size
        resize_mutex.unlock();
        return;
    }

    if(((unsigned int)new_size >= zones[zone].leds_min) && ((unsigned int)new_size <= zones[zone].leds_max))
    {
        if (zones[zone].leds_count < (unsigned int)new_size)
        {
            std::string channel_num = std::to_string(zone + 1);

            // adding leds
            int zoneStartIndex = 0;
            for (int i = 0; i < zone; i++)
            {
                zoneStartIndex += zones[i].leds_count;
            }

            // insert new leds after existing zone[x] leds, but before zone[x+1] leds
            // must make insertions into both the leds and leds_channel vectors
            for (int led_idx = zoneStartIndex + zones[zone].leds_count; led_idx < zoneStartIndex + new_size; led_idx++)
            {
                led new_led;
                new_led.name = ctrl->GetDeviceName().append(" Channel ");
                new_led.name.append(channel_num).append(", LED ").append(std::to_string(led_idx + 1));
                leds.insert(leds.begin() + led_idx, new_led);
                leds_channel.insert(leds_channel.begin() + led_idx, zone);
            }
        } else {
            // removing leds
            int numToRemove = zones[zone].leds_count - new_size;
            int zoneTgtEndIndex = numToRemove;
            for (int i = 0; i < zone; i++)
            {
                zoneTgtEndIndex += zones[i].leds_count;
            }
            leds.erase(leds.begin() + zoneTgtEndIndex, leds.begin() + zoneTgtEndIndex + numToRemove);

        }
        zones[zone].leds_count = new_size;
        SetupColors();
    }
    resize_mutex.unlock();
}

void RGBController_Thermaltake::DeviceUpdateLEDs()
{
    for(std::size_t zone_idx = 0; zone_idx < zones.size(); zone_idx++)
    {
        ctrl->SetRGB(zone_idx, zones[zone_idx].colors, zones[zone_idx].leds_count);
    }
}

void RGBController_Thermaltake::UpdateZoneLEDs(int zone)
{
    ctrl->SetRGB(zone, zones[zone].colors, zones[zone].leds_count);
}

void RGBController_Thermaltake::UpdateSingleLED(int led)
{
    unsigned int channel = leds_channel[led];

    ctrl->SetRGB(channel, zones[channel].colors, zones[channel].leds_count);
}

void RGBController_Thermaltake::SetCustomMode()
{
    active_mode = 0;
}

void RGBController_Thermaltake::DeviceUpdateMode()
{
    for(std::size_t zone_idx = 0; zone_idx < zones.size(); zone_idx++)
    {
        ctrl->SetMode(modes[active_mode].value);
        //ctrl->SetFanSpeed(zone_idx, modes[active_mode].speed);
        ctrl->SetRGB(zone_idx, zones[zone_idx].colors, zones[zone_idx].leds_count);
    }
}
