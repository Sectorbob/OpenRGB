#include "ThermaltakeHeartbeatController.h"

#include <QDebug>

ThermaltakeHeartbeatController::ThermaltakeHeartbeatController(hid_device* dev_handle, const char* path)
{
    wchar_t tmpName[HID_MAX_STR];
    dev         = dev_handle;
    location    = path;
    heartbeat_interval = DEFAULT_HEARTBEAT_INTERVAL;

    // using the controller provided manufacturer string is inconsitent and unreliable
    device_name = "Thermaltake Controller";

    hid_get_serial_number_string(dev, tmpName, HID_MAX_STR);
    std::wstring wName = std::wstring(tmpName);
    serial = std::string(wName.begin(), wName.end());
}

/**
 * Destructor should be overloadable
 *
 * @brief ThermaltakeBaseClass::~ThermaltakeBaseClass
 */
ThermaltakeHeartbeatController::~ThermaltakeHeartbeatController() {
    StopHeartbeat();
    hid_close(dev);
}

void ThermaltakeHeartbeatController::Init() {
    StartHeartbeat();
}

void ThermaltakeHeartbeatController::StartHeartbeat() {
    keepalive_thread_run = 1;
    keepalive_thread = new std::thread(&ThermaltakeHeartbeatController::RunHeartbeat, this);
}

void ThermaltakeHeartbeatController::RunHeartbeat() {
    while(keepalive_thread_run.load())
    {
        if((std::chrono::steady_clock::now() - last_commit_time) > heartbeat_interval)
        {
            //qDebug() << this->GetDeviceName().c_str() << " heartbeat pulse";
            try {
                update_mutex.lock();
                Heartbeat();
                update_mutex.unlock();
            } catch (std::exception e) {
                update_mutex.unlock();
                qDebug() << "heartbeat error" << e.what();
                throw e;
            }

            /*-------------------------------------*\
            | Update the last commit time           |
            \*-------------------------------------*/
            last_commit_time = std::chrono::steady_clock::now();
        }

        std::this_thread::sleep_for(heartbeat_interval / 10);
    }
}

void ThermaltakeHeartbeatController::StopHeartbeat() {
    keepalive_thread_run = 0;
    keepalive_thread->join();
    delete keepalive_thread;
}

std::string ThermaltakeHeartbeatController::GetDeviceName()
{
    return device_name;
}

std::string ThermaltakeHeartbeatController::GetDeviceLocation()
{
    return("HID: " + location);
}

std::string ThermaltakeHeartbeatController::GetSerial()
{
    return(serial);
}

unsigned int ThermaltakeHeartbeatController::NumberOfChannels() {
    return DEFAULT_NUM_ZONES;
}

void ThermaltakeHeartbeatController::SetMode(uint8_t mode) {
    current_mode = mode;
}

// Abstract functions that are not implemented
void ThermaltakeHeartbeatController::Heartbeat () {}

void ThermaltakeHeartbeatController::SetRGB (uint8_t /*channel*/, RGBColor * /*colors*/, unsigned int /*num_colors*/) {}
