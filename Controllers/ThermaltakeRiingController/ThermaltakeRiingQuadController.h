#ifndef THERMALTAKERIINGQUADCONTROLLER_H
#define THERMALTAKERIINGQUADCONTROLLER_H

#include "ThermaltakeHeartbeatController.h"

class ThermaltakeRiingQuadController : public ThermaltakeHeartbeatController
{
private:
    static const int COMMAND_BYTE      = 1;
    static const int FUNCTION_BYTE     = 2;
    static const int ZONE_BYTE         = 3;
    static const int MODE_BYTE         = 4;
    static const int DATA_BYTE         = 5;
    static const int NUM_CHANNELS      = 5;
    static const int PACKET_SIZE       = 193;
    static const int INTERRUPT_TIMEOUT = 250;
    unsigned char               current_mode;
    unsigned char               current_speed;
    std::atomic<uint8_t>        actual_fan_speeds[NUM_CHANNELS];
    std::atomic<unsigned short> actual_rpms[NUM_CHANNELS];
    void GetData(uint8_t port);
public:
    ThermaltakeRiingQuadController(hid_device* dev_handle, const char* path, unsigned short pid);
    ~ThermaltakeRiingQuadController();

    static const uint8_t MODE_DIRECT = 0x24;

    uint8_t     tt_quad_buffer[NUM_CHANNELS][PACKET_SIZE];

    void Init();
    void Heartbeat();
    void SetRGB (unsigned char channel, RGBColor * colors, unsigned int num_colors);
    void SetFanSpeed(uint8_t port, uint8_t speed);
    uint8_t GetActualFanSpeed(uint8_t port);
    uint8_t GetActualRPM(uint8_t port);
};

#endif // THERMALTAKERIINGQUADCONTROLLER_H
