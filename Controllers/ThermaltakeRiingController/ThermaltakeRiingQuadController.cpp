/*-------------------------------------------------------------------*\
|  ThermaltakeRiingQuadController.cpp                                 |
|                                                                     |
|  Driver for Thermaltake Riing Quad Controller                       |
|                                                                     |
|  Chris M (Dr_No)          15th Feb 2021                             |
|  Kyle H  (Sectorbob)      12th Apr 2022                             |
|                                                                     |
\*-------------------------------------------------------------------*/

#include "ThermaltakeRiingQuadController.h"
#include <QDebug>
#include <stdexcept>

ThermaltakeRiingQuadController::ThermaltakeRiingQuadController(hid_device* dev_handle, const char* path, unsigned short pid) : ThermaltakeHeartbeatController(dev_handle, path)
{
    device_name = "Thermaltake Riing Quad Controller (" + std::to_string(pid) + ")";
    current_mode = 0x24;
    current_speed = 0x00;
    /*-----------------------------------------------------*\
    | The Riing Quad only seems to run in direct mode and   |
    | requires a packet within seconds to remain in the     |
    | set mode (similar to Corsair Node Pro). Start a thread|
    | to send a packet every TT_QUAD_KEEPALIVE seconds      |
    \*-----------------------------------------------------*/
    memset(tt_quad_buffer, 0x00, sizeof(tt_quad_buffer));
    unsigned char temp_buffer[3]    = { 0x00, 0x32, 0x52 };

    for(std::size_t zone_index = 0; zone_index < NUM_CHANNELS; zone_index++)
    {
        /*-------------------------------------------------*\
        | Add the constant bytes for the mode info buffer   |
        \*-------------------------------------------------*/
        memcpy(&tt_quad_buffer[zone_index][0], temp_buffer, 3);
    }
}

ThermaltakeRiingQuadController::~ThermaltakeRiingQuadController() {
}

void ThermaltakeRiingQuadController::Init() {
    unsigned char usb_buf[PACKET_SIZE];

    /*-----------------------------------------------------*\
    | Zero out buffer                                       |
    \*-----------------------------------------------------*/
    memset(usb_buf, 0x00, sizeof(usb_buf));

    /*-----------------------------------------------------*\
    | Set up Init packet                                    |
    \*-----------------------------------------------------*/
    usb_buf[0x00]   = 0x00;
    usb_buf[0x01]   = 0xFE;
    usb_buf[0x02]   = 0x33;

    /*-----------------------------------------------------*\
    | Send packet                                           |
    \*-----------------------------------------------------*/
    update_mutex.lock();
    hid_write(dev, usb_buf, PACKET_SIZE);
    hid_read_timeout(dev, usb_buf, PACKET_SIZE, INTERRUPT_TIMEOUT);
    update_mutex.unlock();

    // Set fans to a percent
    uint8_t fan_speed = 75;
    for(std::size_t channel_index = 0; channel_index < NUM_CHANNELS; channel_index++) {
        SetFanSpeed(channel_index, fan_speed);
    }

    ThermaltakeHeartbeatController::Init();
}

/**
 * Same as old SendBuffer(). Except that the last_commmit_time is updated by the thread function wrapper.
 * @brief ThermaltakeRiingQuadController::Heartbeat
 */
void ThermaltakeRiingQuadController::Heartbeat() {
    for(std::size_t channel_index = 0; channel_index < NUM_CHANNELS; channel_index++)
    {
        hid_write(dev, tt_quad_buffer[channel_index], PACKET_SIZE);
        hid_read_timeout(dev, NULL, 0, INTERRUPT_TIMEOUT);
        //GetData(channel_index); for some reason this causes SetRGB updates to fail
    }
}

void ThermaltakeRiingQuadController::SetRGB(unsigned char channel, RGBColor * colors, unsigned int num_colors)
{
    unsigned char* color_data = new unsigned char[3 * num_colors];

    for(unsigned int color = 0; color < num_colors; color++)
    {
        int color_idx = color * 3;
        color_data[color_idx + 0]   = RGBGetGValue(colors[color]);
        color_data[color_idx + 1]   = RGBGetRValue(colors[color]);
        color_data[color_idx + 2]   = RGBGetBValue(colors[color]);
    }

    update_mutex.lock();
    try {
        tt_quad_buffer[channel][ZONE_BYTE]     = channel + 1;
        tt_quad_buffer[channel][MODE_BYTE]     = current_mode + ( current_speed & 0x03 );
        memcpy(&tt_quad_buffer[channel][DATA_BYTE], color_data, (num_colors * 3));
        hid_write(dev, tt_quad_buffer[channel], PACKET_SIZE);
        hid_read_timeout(dev, NULL, 0, INTERRUPT_TIMEOUT);

        update_mutex.unlock();
        delete[] color_data;
    } catch (std::exception e) {
        update_mutex.unlock();
        delete[] color_data;
        throw e;
    }
}

void ThermaltakeRiingQuadController::SetFanSpeed(uint8_t port, uint8_t speed) {
    current_speed = speed;
    unsigned char usb_buf[PACKET_SIZE];

    /*-----------------------------------------------------*\
    | Zero out buffer                                       |
    \*-----------------------------------------------------*/
    memset(usb_buf, 0x00, sizeof(usb_buf));

    /*-----------------------------------------------------*\
    | Set up Set Speed packet                               |
    \*-----------------------------------------------------*/
    usb_buf[0x00]          = 0x00;
    usb_buf[COMMAND_BYTE]  = 0x32;
    usb_buf[FUNCTION_BYTE] = 0x51;
    usb_buf[ZONE_BYTE]     = port + 1;
    usb_buf[MODE_BYTE]     = 0x01;
    usb_buf[DATA_BYTE]     = speed;

    /*-----------------------------------------------------*\
    | Send packet                                           |
    \*-----------------------------------------------------*/
    update_mutex.lock();
    hid_write(dev, usb_buf, PACKET_SIZE);
    hid_read_timeout(dev, NULL, 0, INTERRUPT_TIMEOUT);
    update_mutex.unlock();
}

uint8_t ThermaltakeRiingQuadController::GetActualFanSpeed(uint8_t port) {
    return actual_fan_speeds[port].load();
}
uint8_t ThermaltakeRiingQuadController::GetActualRPM(uint8_t port) {
    return actual_rpms[port].load();
}

void ThermaltakeRiingQuadController::GetData(uint8_t port) {
    unsigned char usb_buf[PACKET_SIZE];

    /*-----------------------------------------------------*\
    | Zero out buffer                                       |
    \*-----------------------------------------------------*/
    memset(usb_buf, 0x00, sizeof(usb_buf));

    /*-----------------------------------------------------*\
    | Set up Get Data packet                               |
    \*-----------------------------------------------------*/
    usb_buf[0x00]          = 0x00;
    usb_buf[COMMAND_BYTE]  = 0x33;
    usb_buf[FUNCTION_BYTE] = 0x51;
    usb_buf[ZONE_BYTE]     = port + 1;

    /*-----------------------------------------------------*\
    | Send packet                                           |
    \*-----------------------------------------------------*/
    //update_mutex.lock();
    int res = hid_write(dev, usb_buf, PACKET_SIZE);
    if (res == -1) {
        update_mutex.unlock();
        throw std::runtime_error("GetData write operation failed on port " + std::to_string(port));
    }

    /*-----------------------------------------------------*\
    | Receive packet                                        |
    \*-----------------------------------------------------*/
    memset(usb_buf, 0x00, sizeof(usb_buf));
    res = hid_read_timeout(dev, usb_buf, PACKET_SIZE, INTERRUPT_TIMEOUT);
    if (res == -1) {
        //update_mutex.unlock();
        throw std::runtime_error("GetData read reply operation failed on port " + std::to_string(port));
    }
    if (res == 0) {
        //update_mutex.unlock();
        throw std::runtime_error("GetData read reply operation timed-out on port " + std::to_string(port));
    }

    /*-----------------------------------------------------*\
    | Parse packet                                          |
    \*-----------------------------------------------------*/
    actual_fan_speeds[port] = usb_buf[4];
    uint8_t rpm_l = usb_buf[5];
    uint8_t rpm_h = usb_buf[6];

    actual_rpms[port] = (rpm_h << 8) + rpm_l;
    qDebug() << "GetData Port" << port << ": Speed:" << actual_fan_speeds[port] << "%" << actual_rpms[port] << "rpm";
    //update_mutex.unlock();
}
