/*-------------------------------------------------------------------*\
|  ThermaltakeRiingQuadController.cpp                                 |
|                                                                     |
|  Driver for Thermaltake Riing Quad Controller                       |
|                                                                     |
|  Chris M (Dr_No)          15th Feb 2021                             |
|                                                                     |
\*-------------------------------------------------------------------*/

#include "ThermaltakeTempController.h"
#include <string>
#include <cstring>
#include <excpt.h>

#include <QDebug>

ThermaltakeTempController::ThermaltakeTempController(hid_device* dev_handle, const char* path, unsigned short pid) : ThermaltakeHeartbeatController(dev_handle, path)
{
    device_name = "Thermaltake Pacific Temperature Controller (" + std::to_string(pid) + ")";
    heartbeat_interval = std::chrono::milliseconds(500);//ms

    // Notes:
    // moshi moshi shows:
    // fe 33 as Init
    // 33 50 as Get firmware version
    // 32 52 is Set RGB
    // 32 51
    // 33 51 is Get Data

    // Connect
    // send    fe 33 <zeros>                                // init
    // receive fe 33                                        // init
    // send    33 50 <zeros>                                // GetFirmwareVersion
    // receive 33 50 01                                     // GetFirmwareVersion
    // 3 second pause (could just be TT software load time)
    // send    32 52 01 18 03 01 (chunk 1)                  // Set RGB
    // send    32 52 01 18 03 02 (chunk 2)                  // Set RGB
    // receive 32 52 fc ()                                  // received 32/52 from cont: 3252fc0000000029200126d20185db25af2578429000c6e2f008a3dffa8012b4300302164fb420030215e8b40003021644c2d9d0a0d082d083d0e0d0d032e526
    // send    32 51 01 01 1e <zeros>
    // receive 32 52 fc ()                                  // received 32/52 from cont: 3252fc0000000029200126d20185db25af2578429000c6e2f008a3dffa8012b4300302164fb420030215e8b40003021644c2d9d0a0d082d083d0e0d0d032e526
    // send    33 51 01 <zeros>                             // Get Data (probably requesting the temp sensor data on port 1
    // send    32 52 01 18 03 01 ( chunk 1)
    // receive 33 51 01 ff 1e

    /*-----------------------------------------------------*\
    | The Riing Quad only seems to run in direct mode and   |
    | requires a packet within seconds to remain in the     |
    | set mode (similar to Corsair Node Pro). Start a thread|
    | to send a packet every TT_QUAD_KEEPALIVE seconds      |
    \*-----------------------------------------------------*/
    memset(tt_out_buffer, 0x00, sizeof(tt_out_buffer));
    memset(tt_in_buffer, 0x00, sizeof(tt_in_buffer));
    memset(tt_out_buffer2, 0x00, sizeof(tt_out_buffer2));
    unsigned char temp_buffer[3]    = { 0x00, 0x32, 0x52 };

    for(std::size_t zone_index = 0; zone_index < THERMALTAKE_TEMP_NUM_CHANNELS; zone_index++)
    {
        /*-------------------------------------------------*\
        | Add the constant bytes for the mode info buffer   |
        \*-------------------------------------------------*/
        memcpy(&tt_out_buffer[zone_index][0], temp_buffer, 3);
    }
}

void ThermaltakeTempController::Init()
{
    unsigned char usb_buf[THERMALTAKE_TEMP_PACKET_SIZE];

    /*-----------------------------------------------------*\
    | Zero out buffer                                       |
    \*-----------------------------------------------------*/
    memset(usb_buf, 0x00, sizeof(usb_buf));

    /*-----------------------------------------------------*\
    | Set up Init packet                                    |
    \*-----------------------------------------------------*/
    usb_buf[0x00]   = 0x00;
    usb_buf[0x01]   = 0xFE;
    usb_buf[0x02]   = 0x33;

    /*-----------------------------------------------------*\
    | Send packet                                           |
    \*-----------------------------------------------------*/
    update_mutex.lock();
    hid_write(dev, usb_buf, THERMALTAKE_TEMP_PACKET_SIZE);
    hid_read_timeout(dev, usb_buf, THERMALTAKE_TEMP_PACKET_SIZE, THERMALTAKE_TEMP_INTERRUPT_TIMEOUT);
    update_mutex.unlock();

    ThermaltakeHeartbeatController::Init();
}

void ThermaltakeTempController::Heartbeat() {
    for(std::uint8_t channel_index = 0; channel_index < THERMALTAKE_TEMP_NUM_CHANNELS; channel_index++)
    {
        try { // write rgb chunk 1
            tt_out_mutex.lock();
            hid_write(dev, tt_out_buffer[channel_index], THERMALTAKE_TEMP_PACKET_SIZE);
            tt_out_mutex.unlock();
        } catch (std::exception e) {
            qDebug() << this->GetDeviceName().c_str() << ": SendBuffer[] write chunk 1 failed: " << e.what();
            tt_out_mutex.unlock();
            continue;
        }

        try { // read rgb chunk 1 response
            tt_in_mutex.lock();
            hid_read_timeout(dev, tt_in_buffer[channel_index], THERMALTAKE_TEMP_PACKET_SIZE, THERMALTAKE_TEMP_INTERRUPT_TIMEOUT);
            tt_in_mutex.unlock();
        } catch (std::exception e) {
            qDebug() << this->GetDeviceName().c_str() << ": SendBuffer[] read chunk 1 resp failed: " << e.what();
            tt_in_mutex.unlock();
            continue;
        }

        try { // write rgb chunk 2
            tt_out_mutex2.lock();
            hid_write(dev, tt_out_buffer2[channel_index], THERMALTAKE_TEMP_PACKET_SIZE);
            tt_out_mutex2.unlock();
        } catch (std::exception e) {
            qDebug() << this->GetDeviceName().c_str() << ": SendBuffer[] write chunk 2 failed: " << e.what();
            tt_out_mutex.unlock();
            continue;
        }

        try {
            tt_in_mutex.lock();
            hid_read_timeout(dev, tt_in_buffer[channel_index], THERMALTAKE_TEMP_PACKET_SIZE, THERMALTAKE_TEMP_INTERRUPT_TIMEOUT);
            tt_in_mutex.unlock();
        } catch (std::exception e) {
            qDebug() << this->GetDeviceName().c_str() << ": SendBuffer[] read chunk 2 resp failed: " << e.what();
            tt_in_mutex.unlock();
            continue;
        }

        //GetData(channel_index + 1);
    }

}

void ThermaltakeTempController::SetRGB(unsigned char channel, RGBColor * colors, unsigned int num_colors)
{
    if (num_colors < 1) {
        // do nothing if there are no leds configured
        return;
    }
    unsigned char* color_data = new unsigned char[3 * num_colors];

    for(std::uint8_t color = 0; color < num_colors; color++)
    {
        uint8_t color_idx = color * 3;
        color_data[color_idx + 0]   = RGBGetGValue(colors[color]);
        color_data[color_idx + 1]   = RGBGetRValue(colors[color]);
        color_data[color_idx + 2]   = RGBGetBValue(colors[color]);
    }
    uint8_t mode = 0x18;

    unsigned char tt_channel = channel + 1;

    // Update buffer 1 & send chunk 1
    try {
        tt_out_mutex.lock();
        tt_out_buffer[channel][THERMALTAKE_TEMP_ZONE_BYTE]      = tt_channel;
        tt_out_buffer[channel][THERMALTAKE_TEMP_MODE_BYTE]      = mode;
        tt_out_buffer[channel][THERMALTAKE_TEMP_MODE_BYTE + 1]  = 0x03;
        tt_out_buffer[channel][THERMALTAKE_TEMP_CHUNK_BYTE]     = 0x01;
        tt_out_buffer[channel][THERMALTAKE_TEMP_CHUNK_BYTE + 1] = 0x00;
        memcpy(&tt_out_buffer[channel][THERMALTAKE_TEMP_DATA_BYTE], color_data, (num_colors * 3));

        unsigned char chunk1_footer_buffer[21]{0x7f, 0x00, 0xff, 0xff, 0x00, 0xff, 0xff, 0x00, 0x7f, 0xff,
                                                0x00, 0x00, 0xff, 0x7f, 0x00, 0xff, 0xff, 0x00, 0x7f, 0xff, 0x00};
        memcpy(&tt_out_buffer[channel][THERMALTAKE_TEMP_PACKET_SIZE-21], chunk1_footer_buffer, 21);
        hid_write(dev, tt_out_buffer[channel], THERMALTAKE_TEMP_PACKET_SIZE);
        tt_out_mutex.unlock();

        hid_read_timeout(dev, NULL, 0, THERMALTAKE_TEMP_INTERRUPT_TIMEOUT);
    } catch (std::exception e) {
        qDebug() << this->GetDeviceName().c_str() << ": SetChannelLEDs chunk 1 failed: " << e.what();
        tt_out_mutex.unlock();
        delete[] color_data;
        return;
    }

    // send chunk 2
    try {
        unsigned char second_chunk[65]{ 0x00, 0x32, 0x52, tt_channel, current_mode, 0x03,
          0x02 , 0x00 , 0x00 , 0xff , 0x00 , 0x00 , 0xff , 0x7f , 0x00 , 0xff , 0xff , 0x00 , 0x7f , 0xff , 0x00 , 0x00,
          0xff , 0x7f , 0x00 , 0xff , 0xff , 0x00 , 0xff , 0xff , 0x00 , 0x7f , 0xff , 0x00 , 0x00 , 0xff , 0x7f , 0x00,
          0xff , 0xff , 0x00 , 0x7f , 0xff , 0x00 , 0x00 , 0xff , 0x00 , 0x00 , 0xff , 0x7f , 0x00 , 0xff , 0xff , 0x00,
          0x7f , 0xff , 0x00 , 0x00 , 0xff , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 , 0x00 };
        tt_out_mutex2.lock();
        memcpy(&tt_out_buffer2[channel], second_chunk, THERMALTAKE_TEMP_PACKET_SIZE);
        hid_write(dev, tt_out_buffer2[channel], THERMALTAKE_TEMP_PACKET_SIZE);
        tt_out_mutex2.unlock();

        hid_read_timeout(dev, NULL, 0, THERMALTAKE_TEMP_INTERRUPT_TIMEOUT);
    } catch (std::exception e) {
        qDebug() << this->GetDeviceName().c_str() << ": SetChannelLEDs chunk 2 failed: " << e.what();
        tt_out_mutex2.unlock();
        delete[] color_data;
        return;
    }
    delete[] color_data;
}

void ThermaltakeTempController::SetFanSpeed(uint8_t port, uint8_t speed) {
    qDebug() << "unimplemented method call ThermaltakeTempController->SetFanSpeed() port: " << port << " speed: " << speed;
}

/*-------------------------------------------------------------------------------------------------*\
| Private packet sending functions.                                                                 |
\*-------------------------------------------------------------------------------------------------*/


void ThermaltakeTempController::GetFirmwareVersion()
{
    unsigned char usb_buf[THERMALTAKE_TEMP_PACKET_SIZE];

    /*-----------------------------------------------------*\
    | Zero out buffer                                       |
    \*-----------------------------------------------------*/
    memset(usb_buf, 0x00, sizeof(usb_buf));

    /*-----------------------------------------------------*\
    | Set up Init packet                                    |
    \*-----------------------------------------------------*/
    usb_buf[0x00]   = 0x00;
    usb_buf[0x01]   = 0x33;
    usb_buf[0x02]   = 0x50;

    /*-----------------------------------------------------*\
    | Send packet                                           |
    \*-----------------------------------------------------*/
    hid_write(dev, usb_buf, THERMALTAKE_TEMP_PACKET_SIZE);
    hid_read_timeout(dev, usb_buf, THERMALTAKE_TEMP_PACKET_SIZE, THERMALTAKE_TEMP_INTERRUPT_TIMEOUT);

    //char buffer [10];
    //int n = std::sprintf(buffer, "%d.%d.%d", usb_buf[3], usb_buf[4], usb_buf[5]);
    //return buffer;
}

void ThermaltakeTempController::GetData(uint8_t port)
{
    unsigned char usb_buf[THERMALTAKE_TEMP_PACKET_SIZE];

    /*-----------------------------------------------------*\
    | Zero out buffer                                       |
    \*-----------------------------------------------------*/
    memset(usb_buf, 0x00, sizeof(usb_buf));

    /*-----------------------------------------------------*\
    | Set up Init packet                                    |
    \*-----------------------------------------------------*/
    usb_buf[0x00]   = 0x00;
    usb_buf[0x01]   = 0x33;
    usb_buf[0x02]   = 0x51;
    usb_buf[0x03]   = port;

    /*-----------------------------------------------------*\
    | Send packet                                           |
    \*-----------------------------------------------------*/
    try {
        hid_write(dev, usb_buf, THERMALTAKE_TEMP_PACKET_SIZE);
        tt_in_mutex.lock();
        hid_read_timeout(dev, tt_in_buffer[port], THERMALTAKE_TEMP_PACKET_SIZE, THERMALTAKE_TEMP_INTERRUPT_TIMEOUT);
        tt_in_mutex.unlock();
    } catch (std::exception e) {
        qDebug() << this->GetDeviceName().c_str() <<  ": GetData() failed: " << e.what();
        tt_in_mutex.unlock();
    }

    //char buffer [10];
    //int n = std::sprintf(buffer, "%d.%d.%d", usb_buf[3], usb_buf[4], usb_buf[5]);
    //return buffer;
}
